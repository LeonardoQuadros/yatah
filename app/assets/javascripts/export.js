$(document).ready( function() {

  $(document).on('click', 'a.export.xlsx', function() {
    var form = '#' + $(this).attr('form_id');
    var url = $(form).attr('action') + '.xlsx?';
    $(form).find('input[type="text"], input[type="hidden"], input[type="email"], input[type="radio"]:checked, input[type="checkbox"]:checked, select').each(function(index) {
      if(index != 0) url += '&';
      url += $(this).attr('name') + '=' + $(this).val();
    });
    url += '&skip_pagination=1';
    window.location = url;
  });

});