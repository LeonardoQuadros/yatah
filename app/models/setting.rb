# RailsSettings Model
class Setting < RailsSettings::Base


	def self.fonts_to_select
		["tt0001m", "tt0320m", "ailerons-typeface", "xenodemon3dital", "goffik-o", "arialic-hollow", "chalk-line-outline", "londrinaoutline-regular", "the-maple-origins", "vector", "fatfreddieshadow"]
	end

end
