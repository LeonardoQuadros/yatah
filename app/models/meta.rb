class Meta < ApplicationRecord

  validates :nome, :descricao, :data_inicio, :data_fim, presence: true

  has_many :submetas, dependent: :destroy


  def status
  	if self.finalizado
  		"<span class='green-text'>Finalizado</span>".html_safe
  	else
  		"<span class='orange-text'>Incompleto</span>".html_safe
  	end
  end

  def porcentagem
  	unless self.data_inicio.blank? or self.data_fim.blank?
  		dividor = self.data_fim.to_date-self.data_inicio.to_date
  		sprintf( "%0.02f", (100/dividor.to_f))
  	end
  end

  def horas_concluidas
  	horas = 0
  	self.submetas.each do |submeta|
  		horas += submeta.horas_finalizadas.to_i
  	end
  	return horas
  end

end