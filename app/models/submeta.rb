class Submeta < ApplicationRecord

  validates :meta_id, presence: true

  belongs_to :meta
  belongs_to :user
  has_many :events, dependent: :destroy


end