class SendMailer < ActionMailer::Base

  default from: "contato@grupofusione.com.br"

  # Enviar link para reset de senha, informações de record e token são geradas pelo device
  def reset_password_instructions(record, token, opts={})
    @token = token
    @resource = record

    mail( to: @resource.email, subject: "Instruções para recuperação de senha") do |format|
      format.html
    end
  end

  def teste(user)
    @user = user
    mail(to: @user.email, subject: 'Sample Email')
  end

  def enviar_email(user, conteudo)
    @user = user
    @conteudo = conteudo
    mail(to: @user.email, subject: 'Sample Email')
  end

  def confirmacao_cadastro(user)
    @user = user
    mail(to: @user.email, subject: 'CUSTOMER DAY FERBASA MINA IPUEIRA - Confirmação de Cadastro')
  end

  
end