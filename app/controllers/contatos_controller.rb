class ContatosController < PublicController



  def create
  	@contato = Contato.new(contato_params)

  	if @contato.save
  		redirect_to request.referrer, notice: "Registro efetuado, entraremos em contato em breve!"
  	else
	    respond_to do |format|
	      format.js
	    end
  	end
  end

	private

    # Only allow a trusted parameter "white list" through.
    def contato_params
    	params.require(:contato).permit(:nome, :email, :celular, :empresa, :plano_id, :mensagem)
    end
end

