class PagesController < PublicController
  layout :resolve_layout


  def home
    @banner_number = rand(1..5)

    @servicos = Servico.all
    @contato = Contato.new
  end

  def login
  end


  private



    def resolve_layout
      case action_name
      when "login"
        "branco"
      else
        "publico"
      end
    end


end
