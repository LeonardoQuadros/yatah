class User::EmailsController < User::UserController

  before_action :set_email, only: [:show, :edit, :update, :destroy]

  # GET /emails
  def index
    conditions = []
    conditions << "emails.id = #{params[:id]}" unless params[:id].blank?
    conditions << "(UPPER(emails.conteudo) LIKE '%#{params[:conteudo]}%') OR (UPPER(emails.descricao) LIKE '%#{params[:conteudo]}%')" unless params[:conteudo].blank?

    @emails = Email.where(conditions.join(' AND ')).order("emails.created_at DESC")

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /emails/1
  def show
  end

  def new
    @email = Email.new
  end

  def edit
  end

  # PATCH/PUT /users/1
  def update
      redirect_to request.referrer, notice: "Não é possível editar uma email já enviado!"
  end

  def create
  	@email = Email.new(email_params)

  	if @email.save
      Contato.where("contatos.email IS NOT NULL").each do |contato|
        SendMailer.delay.enviar_email(contato, @email.conteudo)
      end
      redirect_to request.referrer, notice: "Email Criado!"
    else
  		redirect_to request.referrer, notice: "Não foi possível enviar!"
  	end
  end

  def destroy
    @email.destroy
    
    redirect_to request.referrer, notice: "Email Deletado!"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_email
      @email = Email.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def email_params
      params.require(:email).permit(:conteudo)
    end


end
