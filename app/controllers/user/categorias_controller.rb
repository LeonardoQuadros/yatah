class User::CategoriasController < User::UserController

  before_action :set_categoria, only: [:show, :edit, :update]
  before_action :garantir_autorizacao

  # GET /categorias
  def index
    conditions = []
    conditions << "categorias.id = #{params[:id]}" unless params[:id].blank?

    @categorias = Categoria.where(conditions.join(' AND '))

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /categorias/1
  def show
  end

  def new
    @categoria = Categoria.new
  end

  def edit
  end

  # PATCH/PUT /users/1
  def update
      if @categoria.update(categoria_params)
        redirect_to request.referrer, notice: "Categoria atualizada!"
      else
        render :edit
      end
  end

  def create
    @categoria = Categoria.new(categoria_params)

    @categoria.save
    respond_to do |format|

      @categoria.save
      format.js
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_categoria
      @categoria = Categoria.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def categoria_params
      params.require(:categoria).permit(:nome, :descricao, :ativo)
    end



    def garantir_autorizacao
      authorize! :config,     :categorias
    end


end
