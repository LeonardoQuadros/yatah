class User::UsersController < User::UserController

  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :garantir_autorizacao

  # GET /users
  def index
    conditions = []
    conditions << "users.id = #{params[:id]}" unless params[:id].blank?
    conditions << "(UPPER(users.nome) LIKE '%#{params[:nome]}%')" unless params[:nome].blank?

    @users = User.where(conditions.join(' AND ')).order("users.created_at DESC")

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /users/1
  def show
  end

  def new
    @user = User.new
  end

  def edit
  end


  # PATCH/PUT /users/1
  def update
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password_confirmation].blank?
    respond_to do |format|
      if @user.update(user_params)
        format.js {render inline: "Materialize.toast('Atualizado com sucesso!', 4000, 'green')" }
      else
        format.js {render inline: "Materialize.toast('Falha', 4000, 'red')" }
      end
    end
  end

  def create
    @user = User.new(user_params)
      if @user.save
        redirect_to edit_user_user_path(@user), notice: "User Criado!"
      else
        respond_to do |format|
            format.js {render inline: "Materialize.toast('Falha', 4000, 'red')" }
        end
      end
  end

  def destroy
    @user.destroy
    
    redirect_to request.referrer, notice: "User Deletado!"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:nome, :email, :password, :password_confirmation, :current_password, :cpf, :cnpj, :celular, :telefone, :nacionalidade, :sex, :data_nascimento, :flg_admin, :profissao, :rg, :estado_civil, :location)
    end

    def garantir_autorizacao
      authorize! :config,     :users
    end

end

