class User::SubmetasController < User::UserController

  before_action :set_submeta, only: [:show, :edit, :update, :destroy]


  def update
    if params[:submeta][:horas_finalizadas].to_i >= @submeta.horas.to_i
      @submeta.finalizado = true
      @submeta.user_id = current_user.id
      Event.where(submeta_id: @submeta.id).first.update(finalizado: true, color: "green")
    end
    @submeta.update(submeta_params)

    respond_to do |format|
      if @submeta.finalizado
        format.js {render inline: "Materialize.toast('Atualizado com sucesso!', 4000, 'green'); location.reload();" }
      else
        format.js {render inline: "Materialize.toast('Atualizado com sucesso!', 4000, 'green')" }
      end
    end
  end

  private
    def set_submeta
      @submeta = Submeta.find(params[:id])
    end

    def submeta_params
      params.require(:submeta).permit(:horas, :observacoes, :descricao, :horas_finalizadas)
    end
end