class User::ArquivosController < User::UserController

  before_action :set_arquivo, only: [:show, :edit, :update, :destroy]

  # GET /arquivos
  def index
    conditions = []
    conditions << "arquivos.id = #{params[:id]}" unless params[:id].blank?
    conditions << "(UPPER(arquivos.nome) LIKE '%#{params[:nome]}%')" unless params[:nome].blank?

    @arquivos = Arquivo.where(conditions.join(' AND ')).order("arquivos.created_at DESC")

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /arquivos/1
  def show
  end

  def new
    @arquivo = Arquivo.new
  end

  def edit
  end

  # PATCH/PUT /users/1
  def update

    if @arquivo.update(arquivo_params)

      unless params[:arquivo][:categorias].blank?
        params[:arquivo][:categorias] = params[:arquivo][:categorias]-[""]
        categorias = Categoria.where("categorias.id IN (#{params[:arquivo][:categorias].split(/\W+/).uniq.join(', ')})") 
      end
      unless categorias.blank?
        categorias.each do |categoria|
          @arquivo.categorias << categoria unless @arquivo.categorias.include?categoria
        end
      end

    end



    respond_to do |format|

      format.js
    end
  end

  def create

    @arquivo = Arquivo.new(arquivo_params)
    

    unless params[:arquivo][:categorias].blank?
      params[:arquivo][:categorias] = params[:arquivo][:categorias]-[""]
      categorias = Categoria.where("categorias.id IN (#{params[:arquivo][:categorias].split(/\W+/).uniq.join(', ')})") unless params[:arquivo][:categorias].blank?
    end

    respond_to do |format|

      if @arquivo.save
        unless categorias.blank?
          categorias.each do |categoria|
            @arquivo.categorias << categoria unless @arquivo.categorias.include?categoria
          end
        end
      end
      format.js
    end
  end

  def destroy
    @arquivo.destroy
    
    redirect_to request.referrer, notice: "Arquivo Deletado!"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_arquivo
      @arquivo = Arquivo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def arquivo_params
      params.require(:arquivo).permit(:nome, :descricao, :observacao, {arquivos: []},
                                    itens_attributes: [:id, :arquivo, :thumb, :_destroy])
    end


end
