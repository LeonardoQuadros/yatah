class User::ConfiguracoesController < User::UserController

  def gerais
  end

  def visualizar_home
    @servicos = Servico.all
    @contato = Contato.new
  end

  def gerais_salvar
  	#SEO
    Setting['CONFIGURACOES.site']                           		= params[:site]
    Setting['CONFIGURACOES.title']                           		= params[:title]
    Setting['CONFIGURACOES.descricao']                           	= params[:descricao]
    Setting['CONFIGURACOES.keys']    								= params[:keys]	
  	#Informações
    Setting['CONFIGURACOES.email']    								= params[:email]	
    Setting['CONFIGURACOES.telefone']    								= params[:telefone]	
    Setting['CONFIGURACOES.site_link']    								= params[:site_link]	
    Setting['CONFIGURACOES.facebook']    								= params[:facebook]	
    
  	#sobre
    Setting['YATAH.mensagem']                             = params[:mensagem]
    Setting['YATAH.titulo']                             = params[:titulo]
    Setting['YATAH.conteudo']                             = params[:conteudo]
    Setting['YATAH.titulo_portfolio']                             = params[:titulo_portfolio]
    Setting['YATAH.subtitulo_portfolio']                             = params[:subtitulo_portfolio]
    Setting['YATAH.o_que_fazemos']                             = params[:o_que_fazemos]
    Setting['YATAH.servicos']                             = params[:servicos]
    Setting['YATAH.contato_titulo']                             = params[:contato_titulo]
    Setting['YATAH.contato_conteudo']                             = params[:contato_conteudo]



    Setting['YATAH.font_mensagem']                             = params[:font_mensagem]
    Setting['YATAH.font_size']                             = params[:font_size]
    Setting['YATAH.position']                             = params[:position]
    Setting['YATAH.font_titulo']                             = params[:font_titulo]
    Setting['YATAH.font_titulo_portfolio']                             = params[:font_titulo_portfolio]
    Setting['YATAH.font_subtitulo_portfolio']                             = params[:font_subtitulo_portfolio]
    Setting['YATAH.font_o_que_fazemos']                             = params[:font_o_que_fazemos]
    Setting['YATAH.font_servicos']                             = params[:font_servicos]
    Setting['YATAH.font_contato_titulo']                             = params[:font_contato_titulo]





    respond_to do |format|
      format.js {render inline: "Materialize.toast('Atualizado', 4000, 'green')" }
    end

  end
end
