class User::ServicosController < User::UserController

  before_action :set_servico, only: [:show, :edit, :update, :destroy]
  before_action :garantir_autorizacao

  # GET /servicos
  def index
    conditions = []
    conditions << "servicos.id = #{params[:id]}" unless params[:id].blank?
    conditions << "(UPPER(servicos.nome) LIKE '%#{params[:nome]}%')" unless params[:nome].blank?

    @servicos = Servico.where(conditions.join(' AND ')).order("servicos.created_at DESC")

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /servicos/1
  def show
  end

  def new
    @servico = Servico.new
  end

  def edit
  end

  # PATCH/PUT /servicos/1
  def update
    respond_to do |format|
      if @servico.update(servico_params)
        format.js {render inline: "Materialize.toast('Atualizado com sucesso!', 4000, 'green')" }
      else
        format.js {render inline: "Materialize.toast('Falha', 4000, 'red')" }
      end
    end
  end

  def create
    @servico = Servico.new(servico_params)
      if @servico.save
        redirect_to edit_user_servico_path(@servico), notice: "Serviço Criado!"
      else
        respond_to do |format|
            format.js {render inline: "Materialize.toast('Falha', 4000, 'red')" }
        end
      end
  end

  def destroy
    @servico.destroy
    
    redirect_to request.referrer, notice: "Serviço Deletado!"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_servico
      @servico = Servico.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def servico_params
      params.require(:servico).permit(:nome, :descricao, :icon, :icon_color, :icon_size)
    end

    def garantir_autorizacao
      authorize! :config,     :servicos
    end

end

