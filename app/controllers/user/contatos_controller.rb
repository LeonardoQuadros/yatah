class User::ContatosController < User::UserController

  before_action :set_contato, only: [:show, :edit, :update, :destroy]
  before_action :garantir_autorizacao

  # GET /contatos
  def index
    conditions = []
    conditions << "contatos.id = #{params[:id]}" unless params[:id].blank?
    conditions << "(UPPER(contatos.nome) LIKE '%#{params[:nome]}%')" unless params[:nome].blank?

    @contatos = Contato.where(conditions.join(' AND ')).order("contatos.created_at DESC")

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /contatos/1
  def show
  end

  def new
    @contato = Contato.new
  end

  def edit
  end

  # PATCH/PUT /contatos/1
  def update
    respond_to do |format|
      if @contato.update(contato_params)
        format.js {render inline: "Materialize.toast('Atualizado com sucesso!', 4000, 'green')" }
      else
        format.js {render inline: "Materialize.toast('Falha', 4000, 'red')" }
      end
    end
  end

  def create
    @contato = Contato.new(contato_params)
      if @contato.save
        redirect_to edit_user_contato_path(@contato), notice: "Contato Criado!"
      else
        respond_to do |format|
            format.js {render inline: "Materialize.toast('Falha', 4000, 'red')" }
        end
      end
  end

  def destroy
    @contato.destroy
    
    redirect_to request.referrer, notice: "Contato Deletado!"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contato
      @contato = Contato.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def contato_params
      params.require(:contato).permit(:empresa, :plano_id, :nome, :email, :celular, :mensagem)
    end

    def garantir_autorizacao
      authorize! :config,     :contatos
    end

end

