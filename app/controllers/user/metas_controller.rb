class User::MetasController < User::UserController

  before_action :set_meta, only: [:show, :edit, :update, :destroy]
  before_action :garantir_autorizacao

  # GET /metas
  def index
    conditions = []
    conditions << "metas.id = #{params[:id]}" unless params[:id].blank?
    conditions << "(UPPER(metas.nome) LIKE '%#{params[:nome]}%')" unless params[:nome].blank?

    @metas = Meta.where(conditions.join(' AND ')).order("metas.created_at DESC")

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /metas/1
  def show
  end

  def new
    @meta = Meta.new
  end

  def edit
  end

  # PATCH/PUT /metas/1
  def update
    respond_to do |format|
      if @meta.update(meta_params)

        @meta.submetas.destroy_all
        c = (@meta.data_inicio.to_date..@meta.data_fim.to_date).map{ |e| e.strftime("%d/%m/%Y")  }
        c.each do |data|
           submeta = Submeta.create(meta_id: @meta.id, data: "#{data}", flg_horas: ((@meta.horas.blank? or @meta.horas.to_i.zero?) ? false : true), horas: @meta.horas, descricao: @meta.descricao )  
           Event.create(title: "#{@meta.horas} #{'horas -' if submeta.flg_horas} #{@meta.nome} - #{@meta.porcentagem}%", color: "red", start: (submeta.data.to_date), end: (submeta.data.to_date+1.day), user_id: current_user.id, submeta_id: submeta.id, description: @meta.descricao)
           submeta = nil   
        end

        format.js {render inline: "Materialize.toast('Atualizado com sucesso!', 4000, 'green')" }
      else
        format.js
      end
    end
  end

  def create
    @meta = Meta.new(meta_params)

      if @meta.save
        c = (@meta.data_inicio.to_date..@meta.data_fim.to_date).map{ |e| e.strftime("%d/%m/%Y")  }
        c.each do |data|
           submeta = Submeta.create(meta_id: @meta.id, data: "#{data}", flg_horas: ((@meta.horas.blank? or @meta.horas.to_i.zero?) ? false : true), horas: @meta.horas, descricao: @meta.descricao )     
           Event.create(title: "#{@meta.horas} horas - #{@meta.nome} - #{@meta.porcentagem}%", color: "red", start: (submeta.data.to_date), end: (submeta.data.to_date+1.day), user_id: current_user.id, submeta_id: submeta.id, description: @meta.descricao)
           submeta = nil
        end
        redirect_to edit_user_meta_path(@meta), notice: "Meta Criado!"
      else
        respond_to do |format|
            format.js
        end
      end
  end

  def destroy
    @meta.destroy
    
    redirect_to request.referrer, notice: "Meta Deletada!"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meta
      @meta = Meta.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def meta_params
      params.require(:meta).permit(:nome, :descricao, :publico, :data_inicio, :data_fim, :horas, :user_id )
    end

    def garantir_autorizacao
      authorize! :config,     :metas
    end

end

