Rails.application.routes.draw do

#=============================================================================
# ================================ USER ======================================
#=============================================================================

  namespace :user do

    controller :users do
    end
    resources :users

    controller :servicos do
    end
    resources :servicos

    controller :contatos do
    end
    resources :contatos

    controller :metas do
    end
    resources :metas

    controller :submetas do
    end
    resources :submetas

    controller :events do
    end
    resources :events

    controller :configuracoes do
      get '/configuracoes/gerais' => :gerais, as: :configuracoes_gerais
      get '/configuracoes/visualizar_home' => :visualizar_home, as: :configuracoes_visualizar_home
      post '/configuracoes/gerais_salvar' => :gerais_salvar, as: :configuracoes_gerais_salvar
    end

    controller :pages do
        get '/home' => :home
    end
    root 'pages#home'
  end

#=============================================================================
# ================================ PÚBLICO ===================================
#=============================================================================

  devise_for :users, :controllers => { registrations: 'registrations'}


  authenticated :user do
    root 'user/pages#home', as: :authenticated_root
  end

  controller :users do
  end
  resources :users

  controller :contatos do
  end
  resources :contatos

  controller :pages do
    get '/home' => :home
    get '/login' => :login

  end 

  root 'pages#home'


get '*path' => redirect('/')
end