# -*- encoding : utf-8 -*-


require File.expand_path( '../seeds/usuario_padrao', __FILE__ )



Setting['YATAH.mensagem'] = "MARKETING"
Setting['YATAH.titulo'] = "ACREDITE NA YATAH MARKETING DIGITAL(...)"
Setting['YATAH.conteudo'] = "Além de uma agência de publicidade, queremos ser um parceiro que cresce junto com você, e para isso procuramos analisar, identificar e resolver os problemas da sua empresa, desde melhorar o seu produto, promover sua marca, estabelecer a sua praça e justificar o seu preço. Trabalhamos com a gestão e execução de ações de marketing, comunicação empresarial, visual merchandising, stands, apresentações, promoção de vendas, websites e muito mais."


Setting['YATAH.titulo_portfolio'] 						=	"PORTFÓLIO"
Setting['YATAH.subtitulo_portfolio'] 						=	"SOBRE O NOSSO TRABALHO"
Setting['YATAH.o_que_fazemos'] 						=		"O QUE FAZEMOS"
Setting['YATAH.servicos'] 						=			"SERVIÇOS"
Setting['YATAH.contato_titulo'] 						=			"AGENDE SUA REUNIÃO"
Setting['YATAH.contato_conteudo'] 						=	"Através do nosso planejamento específico sua empresa alcançará todas as metas possíveis. Confie na Yatah web.marketing.lab"


    Setting['YATAH.font_titulo']                             = "tt0001m"
    Setting['YATAH.font_titulo_portfolio']                             = "tt0001m"
    Setting['YATAH.font_subtitulo_portfolio']                             = "tt0001m"
    Setting['YATAH.font_o_que_fazemos']                             = "tt0001m"
    Setting['YATAH.font_servicos']                             = "tt0001m"
    Setting['YATAH.font_contato_titulo']                             = "tt0001m"