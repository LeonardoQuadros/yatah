# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181028001030) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contatos", force: :cascade do |t|
    t.string   "empresa"
    t.integer  "plano_id"
    t.string   "nome"
    t.string   "email"
    t.string   "celular"
    t.text     "mensagem"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["plano_id"], name: "index_contatos_on_plano_id", using: :btree
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "events", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "start"
    t.datetime "end"
    t.string   "color"
    t.boolean  "finalizado"
    t.integer  "user_id"
    t.integer  "submeta_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["submeta_id"], name: "index_events_on_submeta_id", using: :btree
    t.index ["user_id"], name: "index_events_on_user_id", using: :btree
  end

  create_table "metas", force: :cascade do |t|
    t.string   "nome"
    t.text     "descricao"
    t.boolean  "finalizado",  default: false
    t.boolean  "publico",     default: false
    t.string   "data_inicio"
    t.string   "data_fim"
    t.string   "horas"
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["user_id"], name: "index_metas_on_user_id", using: :btree
  end

  create_table "servicos", force: :cascade do |t|
    t.string   "nome"
    t.text     "descricao"
    t.string   "icon"
    t.string   "icon_color", default: "#2196f3"
    t.string   "icon_size",  default: "4rem"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string   "var",                   null: false
    t.text     "value"
    t.integer  "thing_id"
    t.string   "thing_type", limit: 30
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree
  end

  create_table "submetas", force: :cascade do |t|
    t.string   "descricao"
    t.text     "observacoes"
    t.string   "data"
    t.boolean  "finalizado",        default: false
    t.boolean  "flg_horas",         default: false
    t.string   "horas"
    t.string   "horas_finalizadas"
    t.integer  "meta_id"
    t.integer  "user_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["meta_id"], name: "index_submetas_on_meta_id", using: :btree
    t.index ["user_id"], name: "index_submetas_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "ativo",                  default: true
    t.string   "nome"
    t.string   "apelido"
    t.string   "cpf"
    t.string   "cnpj"
    t.string   "rg"
    t.string   "telefone"
    t.string   "celular"
    t.date     "data_nascimento"
    t.string   "nacionalidade"
    t.string   "naturalidade"
    t.string   "estado_civil"
    t.string   "profissao"
    t.string   "remote_ip"
    t.string   "user_agent"
    t.string   "image"
    t.string   "provider"
    t.string   "uid"
    t.string   "location"
    t.string   "sex"
    t.boolean  "flg_admin",              default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
