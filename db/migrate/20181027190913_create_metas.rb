class CreateMetas < ActiveRecord::Migration[5.0]
  def change
    create_table :metas do |t|
    	t.string									:nome
    	t.text										:descricao
    	t.boolean									:finalizado, default: false
    	t.boolean									:publico, default: false

    	t.string									:data_inicio
    	t.string									:data_fim
    	t.string									:horas

    	#===================================================================================

    	t.references 								:user, index: true
    	t.timestamps
    	
    end
  end
end
