class CreateServicos < ActiveRecord::Migration[5.0]
  def change
    create_table :servicos do |t|
    	t.string					:nome
    	t.text						:descricao
    	t.string					:icon
    	t.string					:icon_color, default: "#2196f3"
    	t.string					:icon_size, default: "4rem"

    	t.timestamps
    end
  end
end
