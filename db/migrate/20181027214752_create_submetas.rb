class CreateSubmetas < ActiveRecord::Migration[5.0]
  def change
    create_table :submetas do |t|


    	t.string						:descricao
    	t.text							:observacoes
    	t.string						:data

    	t.boolean						:finalizado, default: false
    	
    	t.boolean						:flg_horas, default: false
    	t.string						:horas
    	t.string						:horas_finalizadas



    	t.references 					:meta, index: true
    	t.references					:user, index: true
    	t.timestamps
    end
  end
end
