class CreateContatos < ActiveRecord::Migration[5.0]
  def change
    create_table :contatos do |t|
    	t.string					:empresa

    	t.references				:plano, index: true

    	t.string					:nome
    	t.string					:email
    	t.string					:celular
    	t.text						:mensagem


    	t.timestamps
    end
  end
end
